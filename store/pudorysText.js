const pudorysText = {
	kavarna: {
		title: 'Kavárna/recepce',
		area: '30 - 60',
		desc: 'Vstupní prostor bude tvořit kavárna s recepcí, kde mají zákazníci možnost zakoupit si občerstvení a zároveň bude postaráno o jejich vstupné do dílny. Personál je připraven poradit a nasměrovat zákazníka kam potřebuje',
	},
	sklad: {
		title: 'Sklad',
		area: '80 - 100',
		desc: 'Materiál ze skladu bude vydáván na požádání a prodáván za běžný metr nebo kg. Prodejem materiálu přímo v dílně je návštěvníkům usnadněna práce a stroje dostávají to co je pro ně nejlepší. Možnost využití vlastního materiálu závisí na domluvě s personálem.',
	},
	grafDilna: {
		title: 'Grafická dílna',
		area: '100 - 154',
		desc: 'V grafické dílně najdeme stroje pro potisk textilu ale i pro tisk na papír. Hlavním strojem bude vlastní sítotiskový karusel Temple pro práci se 6 barvami a tričky zároveň. Dále sublimační tiskárna pro tisk bannerů, plakátů a nejrůznějších propagačních materiálů. V neposlední řadě zde najdeme plotry, řezačky, vazačky, watertransfer a 3D tiskárnu.',
	},
	satna: {
		title: 'Šatna',
		area: '10 - 18',
		desc: 'Šatna s uzamykatelnými skříňkami slouží pro převléknutí do pracovního oblečení.',
	},
	pcUcebna: {
		title: 'PC učebna',
		area: '30 - 48',
		desc: 'Počítačová učebna bude prostor pro workshopy grafiky, webových stránek a dalších. Zájemci mají možnost v době mimo výuku, využívat počítače ke své vlastní tvorbě.',
	},
	atelier: {
		title: 'FOTO/VIDEO ateliér',
		area: '15 - 25',
		desc: 'Kompletní foto / video ateliér bude přístupný veřejnosti na rezervaci, součástí bude například box na 360° produktové fotografie, fotoplátno, fotoaparáty, objektivy, temná komora, nekonečné a zelené pozadí ale také počítače se softwarem na úpravu fotografií a střih videí.',
	},
	lakovna: {
		title: 'Lakovna',
		area: '70 - 85',
		desc: 'Lakovna nabídne lakovací stanici pro dřevěný i kovový materiál. Nabídka základních barev pro sprejové lakování s možností objednání odstínů dle přání zákazníka.',
	},
	siciDilna: {
		title: 'Šicí/kožená dílna',
		area: '80 - 98',
		desc: 'Bude vybavena šicími stroji pro vyšívání a práci s látkami a kůžemi. Nezanedbatelnou součástí bude stroj na vytváření nášivek.',
	},
	drevstrojDilna: {
		title: 'Dřevo dílna/strojní',
		area: '130 - 158',
		desc: 'Dřevo strojní dílna bude jedním ze stěžejních bodů celé dílny, jelikož zde bude vyráběno největší množství komponentů pro další zpracování. Bude obsahovat stroje pro zpracování dřeva jako je formátovací pila, srovnávačka, ponorná pila, přímočará pila, hoblíky a spoustu dalšího.',
	},
	drevrucDilna: {
		title: 'Dřevo dílna/ruční',
		area: '140 - 174',
		desc: 'K dispozici budou vrtačky, brusky, pilníky, dláta a vše další pro jemnější práci se dřevem. V části nazvané modelárna se bude nacházet hrnčířský kruh, vypalovací pec a pomůcky pro práci s keramikou.',
	},
	kovoDilna: {
		title: 'Kovo dílna',
		area: '70 - 80',
		desc: 'Kovo dílna slouží pro opracování, spojování, broušení a frézování kovového materiálu. K ruce budou mít zákazníci vybavení jako jsou svářečky, svářecí stůl, ohýbačky plechu atd.',
	},
};

export default pudorysText;
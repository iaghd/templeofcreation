const mongoose = require('mongoose');

// const DATABASE_CONECTION =  'mongodb://mongo/temple';
const DATABASE_CONECTION =  'mongodb://localhost/temple';

let portfolioSchema = mongoose.Schema({
	name: String
});

const Portfolio = exports.Portfolio = mongoose.model('portfolio', portfolioSchema);

exports.initializeMongo = function() {
	console.log('Trying to connect to ' + DATABASE_CONECTION);
	mongoose.connect(DATABASE_CONECTION, { useNewUrlParser: true });
	mongoose.Promise = global.Promise;

	let db = mongoose.connection;
	db.on('error', console.error.bind(console, 'connection error: We might not be as connected as I thought'));
	db.once('open', function() {
		console.log('We are connected you and I!');
		addRandomPort();
	});
}

let addRandomPort = function() {
	let silence = new Portfolio({
		name: 'Silence' + Math.random()
	});

	silence.save(function functionName(err, fluffy) {
		if (err) {
			return console.log('err');
		} else {
			console.log('New portfolio');
		}
	});
}

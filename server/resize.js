module.exports = function resize(thumbnail, path, format, width, height, save) {
	const fs = require('fs');
	const sharp = require('sharp');
	const readStream = fs.createReadStream(path);
	let transform;
	if (save) {
		transform = sharp(path);
	} else {
		transform = sharp();
	}
	if (format) {
		transform = transform.toFormat(format);
	}
	if (width || height) {
		transform = transform.resize(width, height);
	}
	if (save) {
		transform.toFile(thumbnail,
			(err, info) => {
				console.log('RESULT: ', info);
				console.log('ERR: ', err);
			});
	} else {
		return readStream.pipe(transform);
	}
}
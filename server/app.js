const path = require('path');
const express = require('express');
const app = express();
const fs = require('fs');
const compression = require('compression');
const mailer = require('express-mailer');
const database = require('../database/database');
const resize = require('./resize');

const PORT = process.env.PORT || 5111;

database.initializeMongo();

app.use(express.static('dist'));
app.use(compression({
	filter: function (req, res) {
		return true;
	}
}));
app.use(express.json());
app.use(require('prerender-node').set('prerenderToken', '9kwZXiveLtoqmSbDDMdY'));

app.set('views', __dirname + '/templates');
app.set('view engine', 'jade');

mailer.extend(app, {
	from: 'info@templeofcreation.cz',
	host: 'smtp.templeofcreation.cz', // hostname
	secureConnection: false, // use SSL
	port: 587, // port for secure SMTP
	transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
	auth: {
		user: 'info@templeofcreation.cz',
		pass: 'TemplePVPAM.6'
	}
});


app.get('/api/getFolder/:id', (req, res) => {
	let folder = './store/images/'+req.params.id;
	fs.readdir(folder, (err, files) => {
		let filesAr = [];
		if (files) {
			files.map((file) => {
				if (file.match(/^(.*\.(jpg|jpeg|png)$)?$/) && file.match(/^((?!thumb).)*$/)) {
					filesAr.push(file);
				}
			});
			res.send(filesAr);
		}
	});
});

app.get('/api/store/images/:folder_id/:img_id', function (req, res, next) {
	thumbnail('./store/images', req, res);
});

app.get('/api/store/static/:img_id', function (req, res, next) {
	thumbnail('./store/static', req, res);
});

app.post('/api/sendMail', function (req, res, next) {
	console.log('SENDMAIL:: ', req.body);
	let msg = req.body;
	app.mailer.send('email', {
		to: 'info@templeofcreation.cz',
		subject: msg.subject,
		replyTo: msg.email,
		name: msg.name,
		email: msg.email,
		message: msg.message,
	}, function (err) {
		if (err) {
			// handle error
			console.log(err);
			res.status(400).send(['There was an error sending the email']);
			return;
		}
		res.status(200).send(['Email sent']);
	});
});

app.get('/api/portfolio', function (req, res) {
	database.Portfolio.find(function (err, portfolio) {
		if (err) return console.error(err);
		res.json(portfolio);
	})
});

app.get('*', function(req, res) {
		res.sendFile('./dist/index.html', {root: './'});
});

app.listen(PORT, () => {
	console.log(`Listening on port ${PORT}`);
});

function thumbnail(path, req, res) {
	let fid = req.params && req.params.folder_id ? req.params.folder_id : '';
	let iid = req.params && req.params.img_id ? req.params.img_id : '';

	let filepath = path + (fid ? '/'+fid : '') + (iid ? '/'+iid : '');

	const widthString = req.query.width;
	const heightString = req.query.height;
	const format = req.query.format;
	let width, height;
	if (widthString) {
			width = parseInt(widthString);
	}
	if (heightString) {
			height = parseInt(heightString);
	}

	let thumbnail = path;
	thumbnail += fid ? '/'+fid : '/';
	let formatFilename = '/'+iid.split('.')[0]+(width ? '_' + width : '')+(height ? 'x' + height : '')+(width || height ? '_thumb' : '')+'.'+format;
	let filename = '/'+iid.split('.')[0]+(width ? '_' + width : '')+(height ? 'x' + height : '')+(width || height ? '_thumb' : '')+'.' +iid.split('.')[1];
	thumbnail += format ? formatFilename : filename;

	if (fs.existsSync(thumbnail)) {
		res.sendFile(thumbnail, { root : './' });
	} else {
		res.type(`image/${format || 'png'}`);
		Promise.resolve(resize(thumbnail, filepath, format, width, height, false).pipe(res)).then(
			(result) => {
				resize(thumbnail, filepath, format, width, height, true);
			},
			(err) => {
				console.log('ERR RESIZE', thumbnail);
			}
		);
	}
}
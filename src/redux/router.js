import update from 'immutability-helper';

function updateAppState (state, struct){
	let router = 'main';
	return update(state, {[router]: struct});
}

export const initialState = {
	actual: {},
	prev: {},
};

export default function router(state = initialState, action = {}) {
	switch (action.type) {
		case '@@router/LOCATION_CHANGE': {
			let router = {...state};
			router.prev = state.actual;
			router.actual = action.payload;
			return router;
		}
		default:
			return state;
	}
}
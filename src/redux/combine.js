import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
// import { reducer as notifReducer } from 'redux-notifications';

import reducer from './reducer';
import router from './router';

export default combineReducers({
	routing: routerReducer,
	router: router,
	// notifs: notifReducer,
	main: reducer,
});

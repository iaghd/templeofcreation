import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Slide from '@material-ui/core/Slide';

@connect(
		state => ({
			router: state.router,
		})
)

export default class Transition extends Component {
	static propTypes = {
		dispatch: PropTypes.any,
		router: PropTypes.any,
		children: PropTypes.any,
	};

	constructor(props){
		super(props);
		this.state = {
		};
	}

	render() {
		const {children, router} = this.props;
		let dir = 'up';
		let prev = router && router.prev ? router.prev.pathname : '';
		let act = router && router.actual ? router.actual.pathname : '';
		if (prev) {
			if (prev === '/' && act === '/team') { // TEAM
				dir = 'down';
			} else if (prev === '/team' && act === '/') {
				dir = 'up';
			} else if (prev === '/' && act === '/projekt') { // PROJEKT
				dir = 'left';
			} else if (prev === '/projekt' && act === '/') {
				dir = 'right';
			} else if (prev === '/' && act === '/portfolio') { // PORTFOLIO
				dir = 'right';
			} else if (prev === '/portfolio' && act === '/') {
				dir = 'left';
			} else if (prev === '/' && act === '/dashboard') { // DASHBOARD
				dir = 'up';
			} else if (prev === '/dashboard' && act === '/') {
				dir = 'down';
			}
		}
		return (
			<Slide in={true} direction={dir} mountOnEnter unmountOnExit>
				{...children}
			</Slide>
		);
	}
}
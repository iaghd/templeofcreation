const theme = theme => ({
	container: {
		height: '800px',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		margin: '50px 10px',
	},
	graph: {
		position: 'relative',
		width: '100%',
		height: '100%',
		// border: '2px dashed rgba(234, 181, 67, 0.5)'
	},
	headline: {
		textAlign: 'center',
		fontSize: '2rem',
		color: theme.colors.sand,
		margin: '35px 0 15px 0',
	},
	help: {
		position: 'absolute',
		zIndex: 999,
		top: '50%',
		width: '100%',
		backgroundColor: theme.colors.blackolive,
		opacity: 0.8,
		height: '150px',
		color: theme.colors.sand,
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
	}
});

export default theme;
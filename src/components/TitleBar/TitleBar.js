import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import theme from './TitleBarTheme.js';
import Hidden from '@material-ui/core/Hidden';

@withStyles(theme)

export default class Dashboard extends Component {
	static propTypes = {
		classes: PropTypes.any,
		title: PropTypes.any,
		history: PropTypes.any
	};

	constructor(props){
		super(props);
		this.state = {
		};
	}

	link = (link) => {
		this.props.history.push(link);
	}

	render() {
		const {classes, title} = this.props;
		return (
			<Hidden smDown>
				<div className={classes.dialogBar} onClick={this.link.bind(this, '/')}>
					<div className={classes.title}>{title}</div>
				</div>
			</Hidden>
		);
	}
}
const theme = theme => ({
	pageWrapper: {
		flex: '1 1 auto',
		paddingTop: '20px',
		display: 'flex',
		justifyContent: 'center',
		flexDirection: 'column',
		// overflow: 'scroll',
	},
	pageWrapperR: {
		flex: '1 1 auto',
		// padding: '50px',
		display: 'flex',
		justifyContent: 'center',
		// overflow: 'scroll',
	},
	infoHover: {
		position: 'absolute',
		padding: '10px',
		opacity: 0,
		display: 'flex',
		top: '20px',
		left: '20px',
		bottom: '20px',
		right: '20px',
		color: theme.colors.sand,
		cursor: 'pointer',
		transition: theme.transitions.create(),
		'&:hover': {
			opacity: 1,
			background: 'rgba(49, 47, 47, 0.95)',
		}
	},
	masonryContainer: {
		minWidth: '250px',
		padding: '5px',
		// border: ['2px', 'dashed', theme.colors.blackolive],
	},
	filter: {
		display: 'flex',
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		margin: '10px 0',
		justifyContent: 'center',
		minHeight: '40px',
		color: theme.colors.sand,
	},
	divider: {
		margin: '0 5px',
	},
	filterBtn: {
		cursor: 'pointer',
		transition: theme.transitions.create(),
		padding: '5px',
		'&:hover': {
			opacity: 0.8,
		}
	},
	fSelected: {
		border: ['1px', 'solid', theme.colors.sand],
	},
	thumbnail: {
		objectFit: 'cover',
		objectPosition: '0 0',
		backgroundColor: theme.colors.blackolive80,
	},
});

export default theme;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Link from 'react-router-dom/Link';
import theme from './FooterTheme.js';
import { withStyles } from '@material-ui/core/styles';
import * as reducer from 'redux/reducer';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import MiniLogo from 'Icons/MiniLogo';
import MenuIcon from 'Icons/MenuIcon';
import FacebookIcon from 'Icons/Facebook';
import InstagramIcon from 'Icons/Instagram';
import MailIcon from 'Icons/Mail';
import Hidden from '@material-ui/core/Hidden';

import LogoIcon from 'Icons/Logo';

@connect(
		state => ({
			main: state.main,
		})
)

@withStyles(theme)

export default class Footer extends Component {
	static propTypes = {
		dispatch: PropTypes.any,
		classes: PropTypes.any,
		location: PropTypes.any,
		history: PropTypes.any,
		mobile: PropTypes.any,
	};

	constructor(props){
		super(props);
		this.state = {
			menu: false,
		};
	}

	componentDidMount = () => {
		window.addEventListener('keydown', this.escHandle);
	}

	escHandle = (e) => {
		if (e.key === 'Escape' && this.state.menu) {
			this.openMenu();
		}
	}

	openMenu = () => {
		this.setState({menu: !this.state.menu});
	}

	link = (link) => {
		this.props.history.push(link);
		this.setState({menu: false});
	}

	render() {
		const {classes, location} = this.props;

		let actualLocation = location && location.pathname ? location.pathname : '';

		return (
			<Collapse classes={{container: classes.footerWrapper, entered: classes.footerWrapperEntered, wrapperInner: classes.footerWrapperInner, wrapper: classes.muiWrapper}} in={this.state.menu}>
				<div className={classes.footerContainer}>
					<div className={classes.btn} onClick={this.openMenu}>
						<MiniLogo className={classes.iconLogo}/>
						<MenuIcon className={classes.iconMenu}/>
					</div>
					<div className={classes.footer}>
						<Hidden xsDown>
							<div className={classes.copyright}>{'\u00A9'+'2018'} <br/>Temple of Creation</div>
						</Hidden>
						<a className={classes.socialBtn} href="https://www.facebook.com/templeofcreation" target="blank"><FacebookIcon className={classes.icn}/></a>
						<a className={classes.socialBtn} href="https://www.instagram.com/temple_of_creation/" target="blank"><InstagramIcon className={classes.icn}/></a>
						<div className={classes.socialBtn} onClick={this.link.bind(this, '/kontakty')}><MailIcon className={classes.icn}/></div>
						<div className={classes.social}>
						</div>
					</div>
					<Hidden xsDown>
						<div className={classes.location}>{actualLocation}</div>
					</Hidden>
				</div>
				<div>
					<div onClick={this.link.bind(this, '/vop')} className={classes.vopLink}>Všeobecné obchodní podmínky</div>
				</div>
				<div className={classes.menuContainer} style={{display: this.state.menu ? 'flex' : 'none'}}>
					<ul className={classes.menu}>
						<li><div onClick={this.link.bind(this, '/')} className={classes.link}>HOME</div></li>
						<li><div onClick={this.link.bind(this, '/portfolio')} className={classes.link}>PORTFOLIO</div></li>
						<li><div onClick={this.link.bind(this, '/projekt')} className={classes.link}>PROJEKT</div></li>
						<li><div onClick={this.link.bind(this, '/dashboard')} className={classes.link}>DASHBOARD</div></li>
						<li><div onClick={this.link.bind(this, '/team')} className={classes.link}>TEAM</div></li>
						<li><div onClick={this.link.bind(this, '/kontakty')} className={classes.link}>KONTAKT</div></li>
					</ul>
				</div>
				<Hidden smUp>
					<div className={classes.copyright}>{'\u00A9'+'2018'} <br/>Temple of Creation</div>
				</Hidden>
			</Collapse>
		);
	}
}
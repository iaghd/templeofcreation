const theme = theme => ({
	contactWrapper:{
		justifyContent: 'flex-end',
		[theme.breakpoints.down('sm')]: {
			justifyContent: 'center',
		}
	},
	contacts: {
		padding: '10px',
		border: ['2px', 'dashed', theme.colors.sand],
		color: theme.colors.sand,
		textTransform: 'uppercase',
		maxWidth: '300px',
		flex: '1 1 auto',
	},
	heading: {
		fontSize: '1.4rem',
	},
	message: {
		backgroundColor: theme.colors.sand,
		minHeight: '150px',
		margin: '0 7.45% -20px 7.5%',
		padding: '20px',
	},
	textField: {
		fontSize: '1rem',
		color: theme.colors.blackolive,
	},
	input: {
		fontSize: '1.2rem',
		padding: '0 5px',
		backgroundColor: theme.colors.sand,
		color: theme.colors.blackolive,
		'&:after':{
			display: 'none',
		},
		'&:before':{
			display: 'none',
		},
	},
	stamp: {
		width: '100px',
		height: '100px',
		border: ['2px', 'dashed', theme.colors.sand],
		marginBottom: '20px',
		'&>svg': {
			width: '110px',
		}
	},
	button: {
		marginTop: '20px',
		border: ['2px', 'dashed', theme.colors.blackolive],
	},
	stampimg: {
		height: '100%',
		marginLeft: '10px',
		marginTop: '10px',
	}
});

export default theme;
import React, { Component } from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import animation from './RouteAnimation.scss';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import MobileDetect from 'mobile-detect';
import Loadable from 'react-loadable';

import Home from 'Home/Home';
import Menu from 'Menu/Menu';
import Footer from 'Footer/Footer';
import PortfolioPage from 'Portfolio/PortfolioPage';
import TitleBar from 'TitleBar/TitleBar';

import { MuiThemeProvider, createGenerateClassName, withStyles } from '@material-ui/core/styles';
import JssProvider from 'react-jss/lib/JssProvider';
import jssPreset from 'jss-preset-default';
import { create } from 'jss';
import themeMui from 'theme/themeMui.js';
import theme from './AppTheme.js';
import particlesParams from './particlesParams.js';

function Loading(pr) {
	if (pr.error) {
		return <div>Error! <button onClick={ pr.retry }>Retry</button></div>;
	} else if (pr.pastDelay) {
		return <div>Loading...</div>;
	} else {
		return null;
	}
}

const Projekt = Loadable({
	loader: () => import('Projekt/Projekt'),
	loading: Loading,
	delay: 300,
});

const Kontakty = Loadable({
	loader: () => import('Kontakty/Kontakty'),
	loading: Loading,
	delay: 300,
});

const Portfolio = Loadable({
	loader: () => import('Portfolio/Portfolio'),
	loading: Loading,
	delay: 300,
});

const Pong = Loadable({
	loader: () => import('Pong/Pong'),
	loading: Loading,
	delay: 300,
});

const Iagh = Loadable({
	loader: () => import('Iagh/Iagh'),
	loading: Loading,
	delay: 300,
});

const Team = Loadable({
	loader: () => import('Team/Team'),
	loading: Loading,
	delay: 300,
});

const Dashboard = Loadable({
	loader: () => import('Dashboard/Dashboard'),
	loading: Loading,
	delay: 300,
});

const Particles = Loadable({
	loader: () => import('react-particles-js'),
	loading: Loading,
});

const Terms = Loadable({
	loader: () => import('Terms/Terms'),
	loading: Loading,
});

const generateClassName = createGenerateClassName({
	productionPrefix: 'toc',
});

const jss = create(jssPreset());
@withStyles(theme)

class App extends Component {
	static propTypes = {
		location: PropTypes.any,
		classes: PropTypes.any,
		history: PropTypes.any,
	};

	constructor(props){
		super(props);
		this.state = {
			pong: false,
			mobile: null,
		};
	}

	componentDidMount = () => {
		let mobile = new MobileDetect(window.navigator.userAgent);
		this.setState({mobile: mobile.mobile()});
	}

	pongHandle = () => {
		this.setState({pong: !this.state.pong});
	}

	render() {
		const {pong, mobile, animation} = this.state;
		const {location, classes, history} = this.props;
		const currentKey = location.pathname.split('/')[1] || '/';
		const timeout = { enter: 475, exit: 375 };
console.log('THEME: ', themeMui);
		let bgstyle = {
			backgroundImage: 'url(api/store/static/pattern.svg)',
			backgroundRepeat: 'repeat',
		};
		let background;
		if (mobile) {
			background = (<div className={classes.particles} style={bgstyle}/>);
		} else {
			background = (<Particles className={classes.particles} params={particlesParams}/>);
		}
		let path = location.pathname.split('/');
		return (
			<JssProvider jss={jss} generateClassName={generateClassName}>
				<MuiThemeProvider theme={themeMui}>
					<div>
						{!pong && !mobile && <Menu location={location}/>}
						{currentKey==='/' && <Iagh mobile={mobile} pongHandle={this.pongHandle} pong={pong}/>}
						{pong && <Pong close={this.pongHandle}/>}
						{!pong &&
							<section className={classes.pageMainInner}>
								{(path.length < 3 && path[1]) && <TitleBar title={path[1]} history={history}/>}
								<Switch location={location}>
									<Route exact path='/' component={Home}/>
									<Route exact path='/projekt' component={Projekt}/>
									<Route exact path='/kontakty' component={Kontakty}/>
									<Route exact path='/portfolio' component={Portfolio}/>
									<Route exact path='/dashboard' component={Dashboard}/>
									<Route exact path='/vop' component={Terms}/>
									<Route
										exact
										path={'/portfolio/:id'}
										render={ (props) => <PortfolioPage {...props}/>}
									/>
									<Route exact path='/team' component={Team}/>
								</Switch>
							</section>
						}
						{!pong && <Footer mobile={mobile} location={location} history={history}/>}
						{background}
					</div>
				</MuiThemeProvider>
			</JssProvider>
		);
	}
}

export default withRouter(App);
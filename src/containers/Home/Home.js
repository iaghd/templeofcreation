import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Link from 'react-router-dom/Link';
import theme from './HomeTheme.js';
import { withStyles } from '@material-ui/core/styles';
import * as reducer from 'redux/reducer';
import Transition from 'Transition/Transition';
import Typography from '@material-ui/core/Typography';

import LogoIcon from 'Icons/Logo';
import PinIcon from 'Icons/PinIcon';
import ArrowFIcon from 'Icons/ArrowF';

@connect(
		state => ({
			main: state.main,
		})
)

@withStyles(theme)

export default class Home extends Component {
	static propTypes = {
		dispatch: PropTypes.any,
		classes: PropTypes.any,
	};

	constructor(props){
		super(props);
		this.state = {
			x: 0,
			y: 0,
			textPos: 0,
			text: '',
		};
	}

	componentDidMount = () => {
		this.typewriter('_Think_Design_Make');
	}

	// mouseHandle = (e) => {
	// 	if (!this.state.pong) {
	// 		this.setState({x: e.pageX, y: e.pageY});
	// 		this.moveDiv();
	// 	}
	// }

	moveDiv = () => {
		let [moveX, moveY] = [(this.state.x / -20), (this.state.y / -40)];
		let animDiv = this.animContainer;

		animDiv.style.transform = `translate3d(${moveX / 2}px, ${moveY}px, 0)`;
		animDiv.style.transition = 'cubic-bezier(0.4, 0, 0.2, 1)';
	}

	typewriter = (text) => {
		let iSpeed = 100;
		let iArrLength = text.length;

		let iTextPos = this.state.textPos;
		if (iTextPos < iArrLength) {
			this.setState({textPos: iTextPos+1, text: text.substring(0, iTextPos+1) + '_'});
			setTimeout( () => { this.typewriter(text); }, iSpeed);
		}
	}

	render() {
		const {classes} = this.props;
		return (
			<Transition>
				<div className={classes.homeWrapper}>
					<div className={classes.homeContainer}>
						<div className={classes.animContainer + ' ' + classes.logoContainer} ref={(div) => { this.animContainer = div; }}>
							<LogoIcon className={classes.logo} onClick={this.testHandle}/>
							<Typography variant='caption' color='primary'>Temple of Creation</Typography>
							<Typography color='primary' variant='caption'>{this.state.text}</Typography>
						</div>
					</div>
				</div>
			</Transition>
		);
	}
}
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Transition from 'Transition/Transition';
import {Scrollbars} from 'react-custom-scrollbars';

export default class Team extends Component {
	static propTypes = {};

	render() {

		return (<Transition>
			<div style={{
					color: 'rgb(234, 181, 67)',
					paddingTop: '50px',
					marginBottom: '50px',
					width: '100%'
				}}>
				<Scrollbars>
					<h3>
						V&scaron;eobecn&iacute; obchodn&iacute; podm&iacute;nky společnosti Temple of Creation s. r. o.
					</h3>
					<ol>
						<li>
							<h3>
								&Uacute;vod
							</h3>
						</li>
					</ol>
					<p>
						Tyto obchodn&iacute; podm&iacute;nky upravuj&iacute; z&aacute;vazkov&eacute; smluvn&iacute; poměry mezi zhotovitelem, tj. společnost&iacute; Temple of Creation s.r.o., IČ 06437311, se s&iacute;dlem Ostr&aacute; 2823/28, 616 00 Brno, zapsan&aacute; v obchodn&iacute;m rejstř&iacute;ku veden&eacute;m Krajsk&yacute;m soudem v Brně, odd&iacute;l C, vložka 102076, (d&aacute;le tak&eacute; jako &bdquo;Temple of Creation s.r.o.&ldquo; nebo zhotovitel), a objednatelem, vznikaj&iacute;c&iacute; v souvislosti s podnikatelskou činnost&iacute; zhotovitele. Tyto podm&iacute;nky jsou v&scaron;echny osoby vstupuj&iacute;c&iacute; do obchodn&iacute;ch vztahů se zhotovitelem povinny zn&aacute;t, sezn&aacute;mit se s nimi, a bezv&yacute;hradně je dodržovat. (D&aacute;le tak&eacute; jen Podm&iacute;nky)
					</p>
					<p>
						Objednatel je při komunikaci se zhotovitelem vč. uzav&iacute;r&aacute;n&iacute; smluv, zas&iacute;l&aacute;n&iacute; objedn&aacute;vek atp., povinen uv&aacute;dět skutečn&eacute; a pravdiv&eacute; &uacute;daje vyžadovan&eacute; při uzav&iacute;r&aacute;n&iacute; smlouvy. V př&iacute;padě, že objednatelem je pr&aacute;vnick&aacute; osoba, je povinna tak činit pouze osobou k tomu opr&aacute;vněnou, s uveden&iacute;m skutečn&yacute;ch a pravdiv&yacute;ch &uacute;dajů. Je povinna takt&eacute;ž označit osobu, kter&aacute; pr&aacute;vn&iacute; jedn&aacute;n&iacute;, za pr&aacute;vnickou osobu čin&iacute;, s označen&iacute;m jej&iacute;ho postaven&iacute;. Skutečnost, že za společnost jednala osoba neopr&aacute;vněn&aacute; k takov&eacute;mu jedn&aacute;n&iacute;, nezbavuje objednatele povinnosti plnit sv&eacute; z&aacute;vazky z uzavřen&eacute; smlouvy, ledaže se prok&aacute;že, že jedn&aacute;n&iacute;m t&eacute;to osoby byl sp&aacute;ch&aacute;n trestn&yacute; čin. Objednatel bere na vědom&iacute;, že uveden&iacute; nepravdiv&yacute;ch &uacute;dajů, je vědom&eacute; klam&aacute;n&iacute; zhotovitele, kdy tento bude každ&eacute; takov&eacute; jedn&aacute;n&iacute;, jehož c&iacute;lem by mělo b&yacute;t na &uacute;kor zprostředkovatele se obohatit, či z&iacute;skat jinou v&yacute;hodu uveden&iacute;m jej v omyl, oznamovat př&iacute;slu&scaron;n&yacute;m org&aacute;nům.
					</p>
					<ol>
						<li>
							<h3>
								Definice pojmů
							</h3>
						</li>
					</ol>
					<p>
						<strong>Zhotovitel</strong>

						&ndash; Temple of Creation s.r.o., IČ 06437311, se s&iacute;dlem Ostr&aacute; 2823/28, 616 00 Brno, zapsan&aacute; v obchodn&iacute;m rejstř&iacute;ku veden&eacute;m Krajsk&yacute;m soudem v Brně, odd&iacute;l C, vložka 102076
					</p>
					<p>
						<strong>Objednatel</strong>

						&ndash; Objednatelem může b&yacute;t fyzick&aacute; či pr&aacute;vnick&aacute; osoba, jednaj&iacute;c&iacute; v r&aacute;mci sv&eacute; podnikatelsk&eacute; činnosti, či fyzick&aacute; osoba nepodnikaj&iacute;c&iacute;, jako spotřebitel. Uveden&iacute;m identifikačn&iacute;ho č&iacute;sla osoby (IČ) v r&aacute;mci uzav&iacute;r&aacute;n&iacute; smlouvy d&aacute;v&aacute; objednatel najevo, že smlouvu uzav&iacute;r&aacute; coby podnikatel.
					</p>
					<p>
						<strong>D&iacute;lo</strong>

						&ndash; D&iacute;lem se rozum&iacute; předmět plněn&iacute;, realizovan&yacute; zhotovitelem, na z&aacute;kladě uzavřen&eacute; smlouvy s objednatelem. Rozsah činnost&iacute;, kter&eacute; mohou b&yacute;t předmětem d&iacute;la, je vždy obsažen v aktu&aacute;ln&iacute;m Cen&iacute;ku, kter&yacute; je př&iacute;lohou č. 1 těchto obchodn&iacute;ch Podm&iacute;nek a obsahuje i podrobnou specifikaci D&iacute;la.
					</p>
					<p>
						<strong>Cen&iacute;k</strong>

						&ndash; Cen&iacute;kem se rozum&iacute; př&iacute;loha č. 1 (zak&aacute;zkov&yacute; list), kter&aacute; je ned&iacute;lnou souč&aacute;st&iacute; těchto obchodn&iacute;ch podm&iacute;nek.
					</p>
					<ol>
						<li>
							<h3>
								Uzav&iacute;r&aacute;n&iacute; smlouvy
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Zhotovitel se zaměřuje na poskytov&aacute;n&iacute; reklamn&iacute;ch služeb, od n&aacute;vrhu corporate designu, přes zad&aacute;n&iacute; v&yacute;roby reklamn&iacute;ch předmětů, po zhotoven&iacute; a mont&aacute;ž reklamn&iacute;ch poutačů, n&aacute;pisů apod. Předmětem smlouvy, uzavřen&eacute; mezi zhotovitelem a objednatelem je z&aacute;vazek zhotovitele prov&eacute;st na svůj n&aacute;klad a nebezpeč&iacute; pro objednatele d&iacute;lo a objednatel se zavazuje d&iacute;lo převz&iacute;t a zaplatit zhotoviteli sjednanou cenu. Konkr&eacute;tn&iacute; rozsah a specifikace d&iacute;la jsou obsaženy v Cen&iacute;ku, popř. v samotn&eacute; objedn&aacute;vce.
						</li>
						<li>
							Smlouvu lze mezi objednatelem a zhotovitelem uzavř&iacute;t v&yacute;hradně p&iacute;semně. Za p&iacute;semnou formu uzavřen&iacute; smlouvy se považuje jak smlouva v listinn&eacute; podobě, opatřen&aacute; podpisy obou smluvn&iacute;ch stran, tak i smlouva uzavřen&aacute; formou emailov&eacute; komunikace, n&aacute;sleduj&iacute;c&iacute;m způsobem:
						</li>
						<ol>
							<li>
								Z&aacute;jemce je opr&aacute;vněn učinit zhotoviteli popt&aacute;vku, kdy tuto popt&aacute;vku je opr&aacute;vněn učinit p&iacute;semně v listinn&eacute; podobě či emailem (viz odstavec Doručov&aacute;n&iacute;).
							</li>
							<li>
								V př&iacute;padě, učin&iacute;-li objednatel objedn&aacute;vku emailem, je povinen zhotoviteli v tomto emailu uv&eacute;st mimo uveden&iacute; &uacute;dajů o sv&eacute; osobě a tak&eacute; specifikaci požadovan&eacute;ho d&iacute;la.
							</li>
							<li>
								Zhotovitel je povinen v př&iacute;padě, že bude schopen objedn&aacute;vku požadovanou objednatelem realizovat, přijet&iacute; t&eacute;to objedn&aacute;vky potvrdit emailem, a současně sdělit objednateli cenu, za kterou požadovan&eacute; d&iacute;lo pro objednatele realizuje. Souč&aacute;st&iacute; tohoto potvrzuj&iacute;c&iacute;ho emailu bude mimo jin&eacute; rekapitulace objedn&aacute;vky, obsahuj&iacute;c&iacute; specifikaci d&iacute;la, kter&eacute; objednatel objedn&aacute;v&aacute;, cena kterou je povinen objednatel zhotoviteli uhradit, s popisem toho, jak&eacute; d&iacute;lč&iacute; kroky budou zhotovitelem uskutečněny. Souč&aacute;st&iacute; t&eacute;to rekapitulace může b&yacute;t i položkov&yacute; rozpočet jednotliv&yacute;ch &uacute;konů ze strany zhotovitele.
							</li>
							<li>
								K uzavřen&iacute; smlouvy, a to jak&eacute;koliv nedoch&aacute;z&iacute; dř&iacute;ve, než doručen&iacute;m emailu objednatele, kter&yacute; objednatel v&yacute;slovně odsouhlas&iacute; rekapitulaci objedn&aacute;vky a cenu navrženou zhotovitelem. Souč&aacute;st&iacute; tohoto potvrzen&iacute; mus&iacute; b&yacute;t tak&eacute; v&yacute;slovn&eacute; prohl&aacute;&scaron;en&iacute; objednatele, že se sezn&aacute;mil s těmito Podm&iacute;nkami a s jejich zněn&iacute;m souhlas&iacute;.
							</li>
							<li>
								V př&iacute;padě, učin&iacute;-li z&aacute;jemce popt&aacute;vku telefonicky, sděl&iacute; zhotoviteli stejn&eacute; informace, kter&eacute; je povinen sdělit emailem. Zhotovitel bude n&aacute;sledně postupovat obdobně, jako při objedn&aacute;vce emailem, kdy objednateli emailem potvrd&iacute; obdržen&iacute; objedn&aacute;vky s cenovou nab&iacute;dkou. Emailem, kter&yacute;m objednatel odsouhlas&iacute; rekapitulaci objedn&aacute;vky a cenu navrženou zhotovitelem, doch&aacute;z&iacute; k uzavřen&iacute; smlouvy o d&iacute;lo.
							</li>
							<li>
								V př&iacute;padě, že bude m&iacute;t objednatel v&yacute;hrady k rekapitulaci objedn&aacute;vky, ke smluvn&iacute;m podm&iacute;nk&aacute;m dan&yacute;m zhotovitelem, či bude požadovat změny v ujedn&aacute;n&iacute;ch, je opr&aacute;vněn o takov&eacute; změně jednat, av&scaron;ak každ&aacute; takov&aacute; změna mus&iacute; b&yacute;t zanesena ve formě dodatku k objedn&aacute;vce a tento mus&iacute; b&yacute;t v&yacute;slovně odsouhlasen p&iacute;semně oběma smluvn&iacute;mi stranami. V&yacute;slovně se vylučuje přijet&iacute; nab&iacute;dky s dodatkem nebo odchylkou ve smyslu ust. &sect; 1740 odst. 3 věta prvn&iacute; občansk&eacute;ho z&aacute;kon&iacute;ku.
							</li>
							<li>
								Pro komunikaci se zhotovitelem, je objednatel povinen po celou dobu sjedn&aacute;v&aacute;n&iacute; smlouvy, vč. realizace d&iacute;la, komunikovat z totožn&eacute; emailov&eacute; adresy.
							</li>
							<li>
								Nedojde-li mezi smluvn&iacute;mi stranami k odsouhlasen&iacute; ceny za d&iacute;lo, nen&iacute; smlouva o d&iacute;lo mezi smluvn&iacute;mi stranami uzavřena, a zhotovitel nen&iacute; povinen objednatelem požadovan&eacute; d&iacute;lo realizovat. Tot&eacute;ž plat&iacute; o jak&eacute;koliv jin&eacute; odchylce nebo dodatku učiněn&eacute;m ze strany objednatele.
							</li>
							<li>
								Nebude-li schopen zhotovitel požadovan&eacute; d&iacute;lo zhotovit, sděl&iacute; to objednateli, prost&yacute;m sdělen&iacute;m formou emailu o neakceptaci objedn&aacute;vky.
							</li>
							<li>
								Neobdrž&iacute;-li zhotovitel potvrzuj&iacute;c&iacute; email dle odst. 4 do 1 měs&iacute;ce od zasl&aacute;n&iacute; rekapitulace objedn&aacute;vky spolu s cenou objednateli, nad&aacute;le se k takov&eacute; rekapitulaci nepřihl&iacute;ž&iacute;.
							</li>
						</ol>
						<li>
							V př&iacute;padě, dojde-li mezi smluvn&iacute;mi stranami k uzavřen&iacute; smlouvy formou emailov&eacute; komunikace, a n&aacute;sledně dojde k uzavřen&iacute; p&iacute;semn&eacute; smlouvy v listinn&eacute; podobě mezi smluvn&iacute;mi stranami o t&eacute;mže předmětu plněn&iacute;, zanikaj&iacute; jak&aacute;koliv ujedn&aacute;n&iacute; smluvn&iacute;ch stran sjednan&aacute; formou emailu, a p&iacute;semn&aacute; smlouva v listinn&eacute; podobě v pln&eacute;m rozsahu nahrazuje ve&scaron;ker&aacute; předchoz&iacute; ujedn&aacute;n&iacute;.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Obecn&aacute; ustanoven&iacute; o povinnostech objednatele
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Objednatel se nesm&iacute; nikdy, za ž&aacute;dn&yacute;ch okolnost&iacute; vyd&aacute;vat za jinou osobu, ať už pr&aacute;vnickou nebo fyzickou.
						</li>
						<li>
							Objednatel je vždy povinen poskytovat zhotoviteli ve&scaron;ker&eacute; informace, kter&eacute; zhotovitel potřebuje k realizaci d&iacute;la a poskytovat zhotoviteli ve&scaron;kerou součinnost, kterou po něm zhotovitel v r&aacute;mci realizace d&iacute;la požaduje. Objednatelem poskytovan&eacute; informace mus&iacute; b&yacute;t pravdiv&eacute; a nezkreslen&eacute;, a objednatel nese plnou a v&yacute;lučnou odpovědnost za &scaron;kodu způsobenou komukoliv, i třet&iacute; osobě, sdělen&iacute;m nepravdiv&yacute;ch, ne&uacute;pln&yacute;ch nebo zkreslen&yacute;ch informac&iacute; zhotoviteli. Způsob&iacute;-li zhotovitel objednateli &scaron;kodu z důvodu poru&scaron;en&iacute; povinnosti stanoven&eacute; v tomto odstavci objednatelem, neodpov&iacute;d&aacute; za takto vzniklou &scaron;kodu, a nem&aacute; povinnost k jej&iacute; n&aacute;hradě.
						</li>
						<li>
							V př&iacute;padě, že objednatel poru&scaron;&iacute; povinnosti stanoven&eacute; těmito Podm&iacute;nkami, je zhotovitel opr&aacute;vněn odm&iacute;tnout poskytov&aacute;n&iacute; dal&scaron;&iacute;ch služeb či uzav&iacute;r&aacute;n&iacute; dal&scaron;&iacute;ch smluv s takov&yacute;m objednatelem.
						</li>
						<li>
							Objednatel m&aacute; pr&aacute;vo, aby z&aacute;vazky zhotovitele, ke kter&yacute;m se zhotovitel zav&aacute;že, byly plněny včas, ř&aacute;dně, s odbornou p&eacute;č&iacute;, a tak, aby jedn&aacute;n&iacute;m zhotovitele nevznikala objednateli &scaron;koda.
						</li>
						<li>
							Ž&aacute;dn&aacute; rada nebo informace, ať už &uacute;stn&iacute; nebo p&iacute;semn&aacute;, kterou objednatel z&iacute;sk&aacute; od zhotovitele, nemůže d&aacute;t vzniknout odpovědnosti či z&aacute;vazku, kter&yacute; nen&iacute; v&yacute;slovně stanoven v těchto Podm&iacute;nk&aacute;ch.
						</li>
						<li>
							Pro jak&yacute;koliv z&aacute;vazek zhotovitele vůči objednateli, kter&yacute; nen&iacute; v&yacute;slovně předv&iacute;d&aacute;n těmito Podm&iacute;nkami, mus&iacute; b&yacute;t uzavřena samostatn&aacute; p&iacute;semn&aacute; smlouva opatřen&aacute; vlastnoručn&iacute;mi podpisy obou stran, na t&eacute;že listině.
						</li>
						<li>
							Zhotovitel v&yacute;slovně vylučuje možnost uzavřen&iacute; z&aacute;vazkov&eacute;ho smluvn&iacute;ho poměru jin&yacute;m způsobem, a to i z&aacute;konem předv&iacute;dan&yacute;m, než takov&yacute;m, v r&aacute;mci kter&eacute;ho zhotovitel v&yacute;slovně potvrd&iacute; uzavřen&iacute; smlouvy, v&yacute;slovně za&scaron;le objednateli potvrzen&iacute;, že jeho objedn&aacute;vka byla akceptov&aacute;na a je z&aacute;vazn&aacute;. Neobdrž&iacute;-li objednatel takov&eacute; potvrzen&iacute;, nen&iacute; jak&eacute;koliv ujedn&aacute;n&iacute; mezi smluvn&iacute;mi stranami způsobil&eacute; založit existenci z&aacute;vazkov&eacute;ho smluvn&iacute;ho poměru mezi těmito stranami.
						</li>
					</ol>
					<ol>
						<li>
							<h3 >
								Zhotoven&iacute; d&iacute;la
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Objednatel je povinen zhotoviteli k započet&iacute; s realizac&iacute; d&iacute;la poskytnout ve&scaron;kerou součinnost, kterou po něm zhotovitel k realizaci d&iacute;la požaduje, vč. před&aacute;n&iacute; ve&scaron;ker&eacute; potřebn&eacute; dokumentace, či zpř&iacute;stupněn&iacute; prostor v nemovit&yacute;ch věcech, kter&yacute;ch se předmět d&iacute;la t&yacute;k&aacute;.
						</li>
						<li>
							Objednatel bere na vědom&iacute;, že zhotovitel je opr&aacute;vněn ke zhotoven&iacute; d&iacute;la využ&iacute;t extern&iacute; poradce, techniky, odborn&eacute; pracovn&iacute;ky, či jin&eacute; třet&iacute; osoby, kter&eacute; ke zhotoven&iacute; d&iacute;la uzn&aacute; za vhodn&eacute;. Objednatel s takov&yacute;mto postupem vyjadřuje v&yacute;slovn&yacute; souhlas.
						</li>
						<li>
							Zhotovitel si vyhrazuje pr&aacute;vo zah&aacute;jit pr&aacute;ci na d&iacute;le i tehdy, pokud nem&aacute; od Objednatele kompletn&iacute; doklady a uzn&aacute;-li, že je možn&eacute; d&iacute;lo s dosavadn&iacute;mi podklady od Objednatele již zpracov&aacute;vat.
						</li>
						<li>
							Zhotovitel je opr&aacute;vněn zhotovov&aacute;n&iacute; d&iacute;la pozastavit na dobu, po kterou mu objednatel neposkytuje součinnost, či po kterou je objednatel v prodlen&iacute; s poskytnut&iacute;m podkladů zhotoviteli, či s &uacute;hradou jak&eacute;koliv splatn&eacute; pohled&aacute;vky.
						</li>
						<li>
							Zhotovitel je povinen dodat d&iacute;lo ve lhůtě sjednan&eacute; s objednatelem. Tuto lhůtu m&aacute; zhotovitel pr&aacute;vo jednostranně přiměřeně prodloužit v př&iacute;padě, že se uk&aacute;že potřeba na realizaci d&iacute;la d&eacute;le pracovat. Prodlen&iacute; zhotovitele se zhotoven&iacute;m d&iacute;la nepřekračuj&iacute;c&iacute; sjednanou dobu ke zhotoven&iacute; d&iacute;la, nen&iacute; podstatn&yacute;m poru&scaron;en&iacute;m smlouvy.
						</li>
						<li>
							Spoč&iacute;v&aacute;-li č&aacute;st objedn&aacute;vky objednatele ve zpracov&aacute;n&iacute; grafick&yacute;ch či jin&yacute;ch n&aacute;vrhů, je povinen objednatel nez&aacute;visle na tom, zda si konečn&yacute; n&aacute;vrh se zhotovitelem odsouhlas&iacute; či nikoliv, zaplatit zhotoviteli odměnu za vyhotoven&iacute; n&aacute;vrhu, př&iacute;padně jeho dal&scaron;&iacute; změny, jsou-li v průběhu vyhotovov&aacute;n&iacute; požadov&aacute;ny objednatelem. Cenu těchto činnost&iacute; urč&iacute; Cen&iacute;k zhotovitele, nedohodnou-li se smluvn&iacute; strany jinak.
						</li>
					</ol>
					<ol>
						<li>
							<h3 >
								Před&aacute;n&iacute; d&iacute;la
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Zhotovitel se zavazuje, že d&iacute;lo bude vyhotoveno v souladu s platnou pr&aacute;vn&iacute; &uacute;pravou v kvalitě obvykl&eacute;, a bude obsahovat ve&scaron;ker&eacute; n&aacute;ležitosti požadovan&eacute; př&iacute;slu&scaron;n&yacute;mi pr&aacute;vn&iacute;mi předpisy Česk&eacute; republiky. Zhotovitel je povinen předat objednateli d&iacute;lo zhotoven&eacute; v rozsahu a ve lhůtě sjednan&eacute;m mezi smluvn&iacute;mi stranami. Objednatel je povinen na v&yacute;zvu zhotovitele d&iacute;lo, či jeho d&iacute;lč&iacute; č&aacute;st převz&iacute;t. Bude-li objednatel v prodlen&iacute; s převzet&iacute;m d&iacute;la, či jeho č&aacute;sti, prodlužuje se sjednan&aacute; doba plněn&iacute; o dobu, po kterou byl objednatel v prodlen&iacute; s převzet&iacute;m d&iacute;la, či jeho č&aacute;sti. O před&aacute;n&iacute; d&iacute;la, či jeho d&iacute;lč&iacute; č&aacute;sti se zavazuj&iacute; smluvn&iacute; strany sepsat před&aacute;vac&iacute; protokol v listinn&eacute; podobě, opatřen&yacute; podpisy smluvn&iacute;ch stran. Před&aacute;vac&iacute; protokol je podkladem pro vystaven&iacute; fakturace objednateli. Pokud je d&iacute;lo fakticky před&aacute;no, av&scaron;ak objednatel odm&iacute;tne podepsat před&aacute;vac&iacute; protokol, učin&iacute; o tom zhotovitel z&aacute;znam v před&aacute;vac&iacute;m protokole. D&iacute;lo se považuje za předan&eacute; dnem jeho faktick&eacute;ho před&aacute;n&iacute;. V př&iacute;padě před&aacute;n&iacute; d&iacute;la elektronickou formou, tedy emailem, se d&iacute;lo považuje za předan&eacute; druh&yacute;m dnem po odesl&aacute;n&iacute;, kdy podkladem k fakturaci je kopie emailu, kter&yacute;m bylo d&iacute;lo, či jeho č&aacute;st, odesl&aacute;na a před&aacute;na objednateli.
						</li>
						<li>
							Zhotovitel může průběžně předkl&aacute;dat nedokončen&eacute; d&iacute;lo objednateli k připom&iacute;nkov&aacute;n&iacute; a odsouhlasen&iacute;. V př&iacute;padě, že objednatel do 5ti dnů ode dne předložen&iacute; d&iacute;lč&iacute; č&aacute;sti d&iacute;la k tomu připom&iacute;nkov&aacute;n&iacute; nevznese n&aacute;mitek, m&aacute; se za to, že s průběžn&yacute;m plněn&iacute;m d&iacute;la souhlas&iacute;. K později vznesen&yacute;m n&aacute;mitk&aacute;m, či připom&iacute;nk&aacute;m se nepřihl&iacute;ž&iacute;.
						</li>
						<li>
							Smluvn&iacute; strany sjedn&aacute;vaj&iacute; akceptac&iacute; těchto podm&iacute;nek v&yacute;hradu vlastnick&eacute;ho pr&aacute;va ve smyslu ust. &sect; 2132 a &sect; 2133 občansk&eacute;ho z&aacute;kon&iacute;ku.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Cena a platebn&iacute; podm&iacute;nky
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Cena d&iacute;la se mezi smluvn&iacute;mi stranami stanovuje smluvně, na z&aacute;kladě požadovan&eacute;ho rozsahu a n&aacute;ročnosti d&iacute;la, s přihl&eacute;dnut&iacute;m k dal&scaron;&iacute;m požadovan&yacute;m vlastnostem či specifikac&iacute;m d&iacute;la ze strany objednatele, a je v&yacute;sledkem cenov&eacute; nab&iacute;dky zhotovitele a jej&iacute;ho odsouhlasen&iacute; objednatelem. Nen&iacute;-li cena stanovena smluvně a jedn&aacute;-li se o objedn&aacute;vku plněn&iacute; dle Cen&iacute;ku, je platn&aacute; cena uveden&aacute; v Cen&iacute;ku zhotovitele.
						</li>
						<li>
							Cena za d&iacute;lo obsahuje oceněn&iacute; v&scaron;ech položek nutn&yacute;ch k ř&aacute;dn&eacute;mu splněn&iacute; v&scaron;ech z&aacute;vazků zhotovitele dle t&eacute;to smlouvy, včetně ve&scaron;ker&yacute;ch nutn&yacute;ch n&aacute;kladů s t&iacute;m spojen&yacute;ch (např&iacute;klad cestovn&eacute;ho, n&aacute;kladů na dopravu, apod.). Hotov&eacute; n&aacute;klady vynaložen&eacute; zhotovitelem na realizaci d&iacute;la je povinen objednatel uhradit v pln&eacute; v&yacute;&scaron;i na v&yacute;zvu zhotovitele i v př&iacute;padě, že od smlouvy kter&aacute;koliv ze stran odstoup&iacute;. Tot&eacute;ž plat&iacute; i t&eacute; č&aacute;sti ceny, na kterou před odstoupen&iacute;m od smlouvy zhotoviteli vznikl n&aacute;rok.
						</li>
						<li>
							Zhotovitel je opr&aacute;vněn &uacute;čtovat v&iacute;cen&aacute;klady př&iacute;mo vznikl&eacute; v souvislosti s jedn&aacute;n&iacute;m objednatele, zejm&eacute;na v&yacute;slovn&eacute;ho doobjedn&aacute;n&iacute; těchto v&iacute;ceprac&iacute;, či neposkytnut&iacute;m součinnosti, popř. jin&eacute;ho mařen&iacute; realizace d&iacute;la.
						</li>
						<li>
							Z&aacute;lohovou fakturu (daňov&yacute; doklad) je zhotovitel opr&aacute;vněn objednateli vystavit až do v&yacute;&scaron;e 100% z ceny d&iacute;la, a to kdykoliv ode dne uzavřen&iacute; smlouvy, a objednatel je povinen ji uhradit. Konečnou fakturu (daňov&yacute; doklad) je zhotovitel opr&aacute;vněn vystavit objednateli kdykoliv od okamžiku před&aacute;n&iacute; d&iacute;la objednateli, popř. ode dne před&aacute;n&iacute; d&iacute;lč&iacute; č&aacute;sti d&iacute;la objednateli. Pokud je d&iacute;lo fakticky před&aacute;no objednateli, av&scaron;ak nen&iacute; k takov&eacute;mu před&aacute;n&iacute; vyhotoven před&aacute;vac&iacute; protokol, nem&aacute; absence před&aacute;vac&iacute;ho protokolu vliv na n&aacute;rok zhotovitele na &uacute;hradu ceny d&iacute;la. Objednatel tak nen&iacute; opr&aacute;vněn platbu neprov&eacute;st jen proto, že nebyl vyhotoven před&aacute;vac&iacute; protokol k d&iacute;lu jinak fakticky předan&eacute;mu.
						</li>
						<li>
							Zhotovitel je opr&aacute;vněn fakturu doručit vyjma listinn&eacute; podoby na s&iacute;dlo, či adresu objednatele, tak&eacute; e-mailem objednateli na email uveden&yacute; ve smlouvě, či na email, jehož prostřednictv&iacute;m byla smlouva uzavřena. V př&iacute;padě doručov&aacute;n&iacute; faktury prostřednictv&iacute;m poskytovatele po&scaron;tovn&iacute;ch služeb, se m&aacute; faktura za doručenou 10t&yacute; den ode dne pod&aacute;n&iacute; z&aacute;silky k po&scaron;tovn&iacute; přepravě, v př&iacute;padě doručov&aacute;n&iacute; emailem, se m&aacute; faktura za doručenou dnem n&aacute;sleduj&iacute;c&iacute;m po dni odesl&aacute;n&iacute;. V př&iacute;padě osobn&iacute;ho doručov&aacute;n&iacute; faktury, mus&iacute; b&yacute;t převzet&iacute; faktury potvrzeno opr&aacute;vněnou osobou objednatele jej&iacute;m vlastnoručn&iacute;m podpisem a opatřeno raz&iacute;tkem s uveden&iacute;m data před&aacute;n&iacute;.
						</li>
						<li>
							Splatnost faktur se sjedn&aacute;v&aacute; na 15 dnů ode dne jej&iacute;ho doručen&iacute; objednateli.
						</li>
						<li>
							Za den uskutečněn&iacute; zdaniteln&eacute;ho plněn&iacute; je považov&aacute;n den ř&aacute;dn&eacute;ho před&aacute;n&iacute; d&iacute;la či jeho d&iacute;lč&iacute; č&aacute;sti.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Ukončen&iacute; smlouvy
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Smluvn&iacute; strany jsou opr&aacute;vněny uzavřenou smlouvu ukončit dohodou nebo odstoupen&iacute;m, za sjednan&yacute;ch podm&iacute;nek. Ostatn&iacute; podm&iacute;nky ukončen&iacute; smlouvy smluvn&iacute; strany vylučuj&iacute;.
						</li>
						<li>
							Objednatel m&aacute; pr&aacute;vo od smlouvy odstoupit v př&iacute;padě, že:
						</li>
						<ol>
							<li>
								Zhotovitel po dobu del&scaron;&iacute; než 1 měs&iacute;c nepokračuje s realizac&iacute; d&iacute;la, a na ani přes p&iacute;semnou v&yacute;zvu objednatele v n&iacute; nepokračuje.
							</li>
							<li>
								Zhotovitel prokazatelně předal d&iacute;lo s vadami, kter&eacute; br&aacute;n&iacute; jeho ř&aacute;dn&eacute;mu využit&iacute;, a ani na p&iacute;semnou v&yacute;zvu objednatele tyto vady ve lhůtě 2 měs&iacute;ců neodstranil.
							</li>
						</ol>
						<li>
							Zhotovitel je opr&aacute;vněn od uzavřen&iacute; smlouvy odstoupit v př&iacute;padě, že:
						</li>
						<ol>
							<li>
								Objednatel je v prodlen&iacute; s &uacute;hradou z&aacute;lohy, či d&iacute;lč&iacute; fakturace za předanou č&aacute;st d&iacute;la o v&iacute;ce než 14 dnů
							</li>
							<li>
								Objednatel ani přes v&yacute;zvu zhotovitele neposkytne součinnost, či nepřed&aacute; potřebn&eacute; podklady k realizaci d&iacute;la
							</li>
							<li>
								Objednatel uvede nepravdiv&eacute;, nepřesn&eacute;, nekompletn&iacute;, neaktu&aacute;ln&iacute; nebo zav&aacute;děj&iacute;c&iacute; informace zhotoviteli, či již jednou poskytnut&eacute; informace neaktualizuje.
							</li>
							<li>
								Objednatel neuhrad&iacute; na v&yacute;zvu zhotovitele &scaron;kodu, kterou mu sv&yacute;m jedn&aacute;n&iacute;m způsobil.

							</li>
							<li>
								Objednatel poru&scaron;&iacute; z&aacute;važně kteroukoliv svoji povinnost mu vypl&yacute;vaj&iacute;c&iacute; ze smlouvy či z těchto Podm&iacute;nek.
							</li>
						</ol>
						<li>
							V př&iacute;padě odstoupen&iacute; od smlouvy kteroukoliv smluvn&iacute; stranou, n&aacute;lež&iacute; zhotoviteli odměna za tu č&aacute;st d&iacute;la, kter&eacute; již pro objednatele realizoval, a tak&eacute; ve&scaron;ker&eacute; již zhotovitelem vynaložen&eacute; n&aacute;klady, a to i takov&eacute;, kter&eacute; měly v př&iacute;padě ř&aacute;dn&eacute;ho dokončen&iacute; a před&aacute;n&iacute; d&iacute;la j&iacute;t k t&iacute;ži zhotovitele. Tato č&aacute;st odměny a hotov&eacute; n&aacute;klady vynaložen&eacute; zhotovitelem s realizac&iacute; d&iacute;la, jsou splatn&eacute; do 15 dnů ode dne vy&uacute;čtov&aacute;n&iacute; těchto n&aacute;kladů a odměny objednateli.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Sankce
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Pro př&iacute;pad poru&scaron;en&iacute; smluvn&iacute;ch podm&iacute;nek, nebo podm&iacute;nek stanoven&yacute;ch ve smlouvě, se smluvn&iacute; strany dohodly na těchto sankc&iacute;ch:
						</li>
						<ol>
							<li>
								Objednatel je povinen uhradit zhotoviteli smluvn&iacute; pokutu ve v&yacute;&scaron;i 0,5 % denně z dlužn&eacute; č&aacute;stky, a to v př&iacute;padě prodlen&iacute; objednatele s &uacute;hradou jak&eacute;koliv splatn&eacute; faktury vystaven&eacute; zhotovitelem dle těchto Podm&iacute;nek, či uzavřen&eacute; smlouvy.
							</li>
							<li>
								Objednatel je povinen uhradit smluvn&iacute; pokutu ve v&yacute;&scaron;i 200 000 Kč zhotoviteli v př&iacute;padě, že objednatel obdrž&iacute; n&aacute;vrh grafick&eacute;ho zobrazen&iacute;, např. loga, či designu, a přestože takov&yacute; odm&iacute;tne jako nevyhovuj&iacute;c&iacute;, už&iacute;v&aacute; jej n&aacute;sledně ke sv&eacute; podnikatelsk&eacute;, nebo jin&eacute; činnosti.
							</li>
							<li>
								Objednatel je povinen uhradit zhotoviteli smluvn&iacute; pokutu ve v&yacute;&scaron;i 10 000 Kč v př&iacute;padě, že prokazatelně poru&scaron;&iacute; povinnosti, k nimž se zav&aacute;zal v čl. IV. těchto Podm&iacute;nek, a to za každ&eacute; takov&eacute; poru&scaron;en&iacute;.
							</li>
						</ol>
						<li>
							Uplatněn&iacute;m kter&eacute;koliv z těchto smluvn&iacute;ch pokut, nen&iacute; dotčeno pr&aacute;vo smluvn&iacute;ch stran na n&aacute;hradu &scaron;kody. Ve&scaron;ker&eacute; smluvn&iacute; pokuty jsou splatn&eacute; na v&yacute;zvu opr&aacute;vněn&eacute; smluvn&iacute; strany, v př&iacute;padě denn&iacute; sazby, jsou jednotliv&eacute; jej&iacute; č&aacute;sti splatn&eacute; denně.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Ochrana spotřebitele
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Ustanoven&iacute; tohoto čl&aacute;nku se vztahuj&iacute; na př&iacute;pady, kdy je Objednatel spotřebitelem ve smyslu ust. &sect; 419 z&aacute;kona č. 89/2012 Sb., občansk&yacute; z&aacute;kon&iacute;k (d&aacute;le jen &bdquo;občansk&yacute; z&aacute;kon&iacute;k&ldquo;), resp. ve smyslu ust. &sect; 2 odst. 1 p&iacute;sm. a) z&aacute;kona č. 634/1992 Sb., z&aacute;kon o ochraně spotřebitele, ve zněn&iacute; pozděj&scaron;&iacute;ch předpisů (d&aacute;le jen &bdquo;z&aacute;kon o ochraně spotřebitele&ldquo;). V př&iacute;padě, že se ujedn&aacute;n&iacute; v tomto čl&aacute;nku li&scaron;&iacute;, či jsou v rozporu s jin&yacute;m ujedn&aacute;n&iacute;m v těchto obchodn&iacute;ch Podm&iacute;nk&aacute;ch, a Objednatelem je spotřebitel, maj&iacute; ustanoven&iacute; tohoto čl&aacute;nku aplikačn&iacute; přednost.
						</li>
						<li>
							Pr&aacute;va a povinnosti stran ohledně pr&aacute;v z vadn&eacute;ho plněn&iacute; se ř&iacute;d&iacute; př&iacute;slu&scaron;n&yacute;mi obecně z&aacute;vazn&yacute;mi pr&aacute;vn&iacute;mi předpisy (zejm&eacute;na ustanoven&iacute;mi &sect; 1914 až &sect; 1925, &sect; 2099 až &sect; 2117, &sect; 2615 až &sect; 2619 občansk&eacute;ho z&aacute;kon&iacute;ku a z&aacute;konem o ochraně spotřebitele).
						</li>
						<li>
							Zhotovitel je povinen prov&eacute;st D&iacute;lo v souladu s touto Smlouvou, a to bez vad s dohodnut&yacute;mi vlastnostmi. Je-li předmětem D&iacute;la zhotoven&iacute; energetick&eacute;ho auditu či jin&eacute; dokumentace slouž&iacute;c&iacute; jako podklad k dal&scaron;&iacute; realizaci (např. posudky, n&aacute;vrhy ře&scaron;en&iacute;, projektov&aacute; dokumentace, apod.) odpov&iacute;d&aacute; Zhotovitel pouze za př&iacute;padn&eacute; vady tohoto D&iacute;la, nikoliv za jak&eacute;koliv vady či vzniklou &scaron;kodu souvisej&iacute;c&iacute; se samotnou realizac&iacute; činnost&iacute; vypl&yacute;vaj&iacute;c&iacute;ch ze Zhotovitelem zhotoven&eacute;ho D&iacute;la.

						</li>
						<li>
							Objednatel je povinen vytknout vadu D&iacute;la bez zbytečn&eacute;ho odkladu pot&eacute;, kdy měl možnost vadu zjistit, a to buďto označen&iacute;m vady, nebo ozn&aacute;men&iacute;m, jak se projevuje. Objednatel je opr&aacute;vněn pr&aacute;va z vadn&eacute;ho plněn&iacute; uplatnit u Zhotovitele e-mailem na adrese info@templeofcreation.cz nebo p&iacute;semnou reklamac&iacute; doručenou na adresu s&iacute;dla Zhotovitele. V r&aacute;mci reklamace je Objednatel povinen p&iacute;semně sdělit Zhotoviteli volbu pr&aacute;va z vadn&eacute;ho plněn&iacute;, kter&eacute; uplatňuje vůči Zhotoviteli.
						</li>
						<li>
							Pro vyř&iacute;zen&iacute; reklamace se uplatn&iacute; z&aacute;kon o ochraně spotřebitele; lhůta pro vyř&iacute;zen&iacute; reklamace je 30 dnů ode dne uplatněn&iacute; reklamace.
						</li>
						<li>
							Objednatel (spotřebitel) v&yacute;slovně souhlas&iacute; se započet&iacute;m plněn&iacute; ze Smlouvy, tj. s realizac&iacute; D&iacute;la, před uplynut&iacute;m lhůty pro odstoupen&iacute; od Smlouvy. Objednatel je srozuměn s t&iacute;m, že v tomto př&iacute;padě nem&aacute; pr&aacute;vo na odstoupen&iacute; od smlouvy dle ust. &sect; 1837 p&iacute;sm. a) občansk&eacute;ho z&aacute;kon&iacute;ku.
						</li>
						<li>
							Objednatel (spotřebitel) m&aacute; pr&aacute;vo na mimosoudn&iacute; ře&scaron;en&iacute; spotřebitelsk&eacute;ho sporu. Subjektem zaji&scaron;ťuj&iacute;c&iacute;m mimosoudn&iacute; ře&scaron;en&iacute; spotřebitelsk&yacute;ch sporů je Česk&aacute; obchodn&iacute; inspekce (www.coi.cz).
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Ochrana dle autorsk&eacute;ho z&aacute;kona
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Tento čl&aacute;nek Podm&iacute;nek se vztahuje na ty Smlouvy uzav&iacute;ran&eacute; s Objednatelem, jejichž předmětem je D&iacute;lo, kter&eacute; je nebo může b&yacute;t, byť z č&aacute;sti, d&iacute;lem autorsk&yacute;m ve smyslu ust. &sect; 2 z&aacute;kona č. 121/2000 Sb., z&aacute;kon o pr&aacute;vu autorsk&eacute;m, o pr&aacute;vech souvisej&iacute;c&iacute;ch s pr&aacute;vem autorsk&yacute;m a o změně někter&yacute;ch z&aacute;konů (d&aacute;le jen &bdquo;autorsk&yacute; z&aacute;kon&ldquo;).
						</li>
						<li>
							Objednatel bere na vědom&iacute;, že nen&iacute; opr&aacute;vněn jakkoliv zasahovat do autorsk&yacute;ch pr&aacute;v, kter&eacute; m&aacute; Zhotovitel k předmětn&eacute;mu D&iacute;lu. Objednatel zejm&eacute;na nen&iacute; opr&aacute;vněn už&iacute;vat Zhotovitelem zhotoven&eacute; D&iacute;lo už&iacute;vat nad r&aacute;mec sjednan&yacute; se Zhotovitelem.
						</li>
						<li>
							Př&iacute;padn&aacute; licenčn&iacute; pr&aacute;va dle ust. &sect; 61 autorsk&eacute;ho z&aacute;kona přech&aacute;zej&iacute; po před&aacute;n&iacute; d&iacute;la realizovan&eacute;ho na z&aacute;kladě t&eacute;to smlouvy na Objednatele. Objednatel nen&iacute; opr&aacute;vněn už&iacute;t a poskytnout licenci k D&iacute;lu jin&eacute;mu subjektu.
						</li>
						<li>
							Objednatel je povinen už&iacute;vat d&iacute;lo pouze způsobem a v r&aacute;mci sjednan&eacute;m mezi Objednatelem a Zhotovitelem. Jak&eacute;koliv neopr&aacute;vněn&eacute; už&iacute;v&aacute;n&iacute; nebo nakl&aacute;d&aacute;n&iacute; s autorsk&yacute;m d&iacute;lem, tedy např. jeho modifikace či vyd&aacute;v&aacute;n&iacute; d&iacute;la za sv&eacute; ze strany Objednatele je z&aacute;sahem do autorsk&yacute;ch pr&aacute;v Zhotovitele.
						</li>
						<li>
							Objednatel se zavazuje při už&iacute;v&aacute;n&iacute; D&iacute;la respektovat ve&scaron;ker&aacute; z&aacute;konn&aacute; ustanoven&iacute; vypl&yacute;vaj&iacute;c&iacute; z autorsk&eacute;ho z&aacute;kona, zejm&eacute;na t&yacute;kaj&iacute;c&iacute; se ochrany d&iacute;la, pr&aacute;v a povinnost&iacute; uživatele d&iacute;la, a pr&aacute;v autora, kter&aacute; m&aacute; k autorsk&eacute;mu d&iacute;lu. Objednatel se zavazuje zajistit, aby ani jin&eacute; osoby, kter&eacute; budou m&iacute;t k d&iacute;lu skrze uživatele př&iacute;stup, do pr&aacute;v autorsk&yacute;ch nezasahovali. Objednatel bere t&iacute;mto na vědom&iacute;, že poru&scaron;en&iacute;m těchto z&aacute;konn&yacute;ch ustanoven&iacute; se vystavuje postihu jak v rovině občanskopr&aacute;vn&iacute;, tak v rovině přestupku, popř. spr&aacute;vn&iacute;ho deliktu, tak v možn&eacute; rovině trestněpr&aacute;vn&iacute;.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Doručov&aacute;n&iacute;
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Objednatel a Zhotovitel se vz&aacute;jemně zavazuj&iacute; spolu komunikovat v&yacute;hradně p&iacute;semně. Za p&iacute;semnou komunikaci se pro &uacute;čely těchto Podm&iacute;nek považuje:
						</li>
						<ol>
							<li>
								Doručov&aacute;n&iacute; p&iacute;semnost&iacute; na adresu Zhotovitele uvedenou v shora v těchto Podm&iacute;nk&aacute;ch, či na email určen&yacute; ke komunikaci s Objednatelem: info@templeofcreation.cz.
							</li>
							<li>
								Doručov&aacute;n&iacute; na adresu Objednatele či na email Objednatele použit&yacute; při sjedn&aacute;v&aacute;n&iacute; Smlouvy, popř. při jej&iacute;m uzavřen&iacute;.
							</li>
						</ol>
						<li>
							Objednatel i Zhotovitel jsou povinni b&yacute;t na těchto adres&aacute;ch, resp. emailech kontaktn&iacute;. V př&iacute;padě z&aacute;silky doručovan&eacute; prostřednictv&iacute;m provozovatele po&scaron;tovn&iacute;ch služeb, se m&aacute; za to, že z&aacute;silka je doručena 10. dnem ode dne odesl&aacute;n&iacute; na adresu druh&eacute; strany. V př&iacute;padě emailov&eacute; komunikace se za den doručen&iacute; považuje den n&aacute;sleduj&iacute;c&iacute; po tom, v němž byl email odesl&aacute;n druh&eacute; straně.
						</li>
						<li>
							Kontaktn&iacute; adresy pro doručov&aacute;n&iacute; jsou povinny si strany ozn&aacute;mit p&iacute;semně. Každ&aacute; ze stran je povinna toto ohl&aacute;&scaron;en&iacute; respektovat. V př&iacute;padě změny emailu Objednatele, je Objednatel povinen tuto změnu p&iacute;semně ozn&aacute;mit.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Prorogačn&iacute; doložka a volba pr&aacute;va
							</h3>
						</li>
					</ol>
					<ol>
						<li>
							Objednatel uhrazen&iacute;m jak&eacute;koliv faktury v&yacute;slovně sjedn&aacute;v&aacute; se Zhotovitelem, že ve&scaron;ker&eacute; spory budou smluvn&iacute; strany ře&scaron;it před věcně a m&iacute;stně př&iacute;slu&scaron;n&yacute;m obecn&yacute;m soudem Česk&eacute; republiky. Ve smyslu ust. &sect; 89a, z&aacute;k. č. 99/1963 Sb., občansk&eacute;ho soudn&iacute;ho ř&aacute;du v platn&eacute;m zněn&iacute;, sjedn&aacute;vaj&iacute; m&iacute;stně př&iacute;slu&scaron;n&yacute; soud v prvn&iacute;m stupni jako Městsk&yacute; soud v Brně.
						</li>
						<li>
							Objednatel bere na vědom&iacute;, a vyjadřuje s t&iacute;m bezv&yacute;hradn&yacute; souhlas, že pr&aacute;vn&iacute; vztahy vznikl&eacute; mezi Objednatelem a Zhotovitelem ze smlouvy, stejně jako v&yacute;klad těchto Podm&iacute;nek, či v&yacute;klad ot&aacute;zek těmito Podm&iacute;nkami neupraven&yacute;mi, se bude ř&iacute;dit v&yacute;lučně platn&yacute;m pr&aacute;vem Česk&eacute; republiky.
						</li>
					</ol>
					<ol>
						<li>
							<h3>
								Z&aacute;věrečn&aacute; a společn&aacute; ustanoven&iacute;
							</h3>
						</li>
					</ol>
					<p>
						Tyto Podm&iacute;nky nab&yacute;vaj&iacute; platnosti a &uacute;činnosti dnem 1. 6. 2018., a jsou z&aacute;vazn&eacute; pro v&scaron;echny osoby vstupuj&iacute;c&iacute; do pr&aacute;vn&iacute;ch vztahů se zhotovitelem, na kter&eacute; dopadaj&iacute;. Každ&aacute; osoba je povinna se s nimi sezn&aacute;mit kdykoliv j&iacute; budou objednatelem předloženy. Za předložen&iacute; obchodn&iacute;ch Podm&iacute;nek se považuje jejich odesl&aacute;n&iacute; emailem ve formě př&iacute;lohy, popř. odesl&aacute;n&iacute; webov&eacute;ho odkazu, na kter&eacute;m jsou dostupn&eacute; v elektronick&eacute; podobě. K p&iacute;semně uzav&iacute;ran&eacute; smlouvě, budou objednateli tyto obchodn&iacute; Podm&iacute;nky předloženy k prostudov&aacute;n&iacute; v p&iacute;semn&eacute; podobě.
					</p>
				</Scrollbars>
			</div>
		</Transition>);
	}
}
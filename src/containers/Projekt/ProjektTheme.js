const theme = theme => ({
	introBar: {
		width: '150px',
		height: '25px',
		background: theme.colors.blackolive,
		position: 'absolute',
		color: theme.colors.sand,
		display: 'flex',
		alignItems: 'center',
		padding: '5px',
		left: 0,
		top: 0,
		'&:after': {
			content: '""',
			position: 'absolute',
			bottom: '-25px',
			left: '0',
			width: '0',
			height: '0',
			borderRight: '150px solid transparent',
			borderTop: ['25px', 'solid', theme.colors.blackolive],
		},
	},
	title: {
		fontSize: '1.4rem',
		textWrap: 'wrap',
		paddingLeft: '20px',
	},
	introduction: {
		position: 'relative',
		flex: '1 1 auto',
		marginTop: '40px',
		padding: '50px 20px 20px 20px',
		backgroundColor: theme.colors.sand,
		[theme.breakpoints.down('sm')]: {
			marginTop: '0',
		}
	},
	teamimg: {
		width: '100%',
		height: '500px',
		objectFit: 'cover'
	},
	infoPudorysContainer: {
		flex: '1 1 auto',
		backgroundColor: theme.colors.sand,
		color: theme.colors.blackolive,
		padding: '10px',
		position: 'relative',
	},
	pudorysBar: {
		width: '50px',
		height: '50px',
		background: theme.colors.blackolive,
		position: 'absolute',
		color: theme.colors.sand,
		display: 'flex',
		alignItems: 'center',
		left: 0,
		top: 0,
		'&:after': {
			content: '""',
			position: 'absolute',
			bottom: '-10px',
			left: '0',
			width: '0',
			height: '0',
			borderRight: '50px solid transparent',
			borderTop: ['10px', 'solid', theme.colors.blackolive],
		},
	},
	pudorysTitle: {
		marginLeft: '60px',
		fontSize: '1.2rem',
		textTransform: 'uppercase',
	},
	pudorysArea: {
		marginLeft: '60px',
		top: '2rem',
		fontSize: '0.8rem',
	},
	text: {
		padding: '10px',
	},
	legend: {
		color: theme.colors.sand,
		animation: 'fadeIn 275ms cubic-bezier(0.4, 0.0, 0.2, 1)',
	},
	svgClass: {
		width: '100%',
		objectFit: 'contain',
		height: '400px',
		animation: 'fadeIn 275ms cubic-bezier(0.4, 0.0, 0.2, 1)',
	},
	svgWrapper: {
		height: '400px',
	},
	'@keyframes fadeIn': {
		'0%': {
			opacity: 0,
		},
		'100%': {
			opacity: 1,
		},
	},
});

export default theme;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Link from 'react-router-dom/Link';
import theme from './DashboardTheme.js';
import { withStyles } from '@material-ui/core/styles';
import * as reducer from 'redux/reducer';
import Typography from '@material-ui/core/Typography';
import Transition from 'Transition/Transition';

import LogoIcon from 'Icons/Logo';
import PinIcon from 'Icons/PinIcon';
import ArrowFIcon from 'Icons/ArrowF';

@connect(
		state => ({
			main: state.main,
		})
)

@withStyles(theme)

export default class Dashboard extends Component {
	static propTypes = {
		dispatch: PropTypes.any,
		classes: PropTypes.any,
		history: PropTypes.any,
	};

	constructor(props){
		super(props);
		this.state = {
		};
	}

	mouseEnter = (type) => {
		if (!this.state[type]) {
			this.setState({[type]: true});
			setTimeout(() => { this.setState({[type]: false}); }, 2000);
		}
	}

	link = (link) => {
		this.props.history.push(link);
	}

	render() {
		const {classes} = this.props;

		return (
			<Transition>
				<div className={classes.homeWrapper}>
						<div className={classes.infoWrapper}>
							{/* <Link to={'/'} className={classes.infoContainer + ' ' + (this.state.pujcovna ? classes.animInfoContainer : '')} onMouseEnter={this.mouseEnter.bind(this,'pujcovna')}>
							<PinIcon className={classes.pinIcon}/>
							<Typography align='center'>Půjčovna</Typography>
							<div className={classes.animIconContainer}>
							<div className={classes.animIcon} style={{backgroundImage: 'url(\'/store/static/camera_sprite.png\')'}}/>
							</div>
						</Link> */}
						<div onClick={this.link.bind(this, '/kontakty')} className={classes.infoContainer + ' ' + (this.state.kontakt ? classes.animInfoContainer : '')} onMouseEnter={this.mouseEnter.bind(this,'kontakt')}>
							<PinIcon className={classes.pinIcon}/>
							<Typography align='center'>Kontakty</Typography>
							<div className={classes.animIconContainer}>
								<div className={classes.animIcon} style={{backgroundImage: 'url(\'/api/store/static/phone_sprite.png\')'}}/>
							</div>
						</div>
					</div>
				</div>
			</Transition>
		);
	}
}